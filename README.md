# Eduardo Bujak
 **adoisjota: coding challenge**

 **O Desafio**

 O desafio que você deverá desenvolver é o frontend de um portal web.

 Você não precisará desenvolver o backend. O mesmo está pronto e disponível para você consumir no frontend.

 Você deverá utilizar a linguagem AngularJS para o frontend.

 **Considerações Gerais**
 
 Você deverá usar este repositório como o principal do projeto. Todos os seus commits devem estar registrados aqui.

 O primeiro material de apoio é como utilizar o Bitbucket. Após você criar sua conta, nos informe o usuário para adicionarmos as permissões neste repositório.

 Você receberá um invite para uma pasta do Google Drive. É um local onde você pode utilizar para guardar algo que julgue necessário.

 **Registre tudo**: Ideias que gostaria de implementar se tivesse mais tempo (explique como você as resolveria), decisões tomadas e seus porquês, arquiteturas testadas e os motivos de terem sido modificadas ou abandonadas. Queremos ver a evolução das suas ideias.

 Sinta-se livre para incluir ferramentas e bibliotecas open source.

 Em caso de dúvidas, pergunte!

 **Materiais de apoio**

 - Como utilizar o Bibucket:
    - https://www.youtube.com/watch?v=s-u0hxQnVzU

 - Angular:
    - https://angular.io/guide/quickstart

 - Padrão REST:
    - https://becode.com.br/o-que-e-api-rest-e-restful/
    - http://blog.caelum.com.br/rest-principios-e-boas-praticas/
    - https://pt.stackoverflow.com/questions/45783/o-que-é-rest-e-restful

 - Versionamento de código:
    - https://git-scm.com/book/pt-br/v1/Primeiros-passos

 - Para testar os endpoints:
    - Insomnia (https://insomnia.rest)

# O Portal

 O portal deve possuir duas telas: uma para a listagem de usuários e outra para criar um novo usuário.

 **Tela Inicial**

 - Lista de Usuários com os campos:
    - Nome do usuário
    - E-mail do usuário

 - Botão para ir para a tela de novo usuário

 **Tela Novo Usuário**

 - Campos:
    - Nome do usuário
    - E-mail do usuário
    - Senha do usuário
    - Botão para salvar o usuário

 Ao clicar em salvar o usuário, deve ser direcionado para a listagem de usuários e o usuário recém criado deve aparecer nesta listagem.

# O Backend

 **Listagem de usuários**

 - Endpoint: http://probi-api-dev.a2j.tech/api/users
 - Método: GET
 
 **Criar usuário**

 - Endpoint: http://probi-api-dev.a2j.tech/api/users
 - Método: POST
 - Payload: (sempre enviar o atributo "role" com "master")
 - {
 -  "name":"Nome do usuário",
 -	"email":"email@dousuario.com",
 -	"password":"SenhaDoUsuário",
 -	"role":"master"
 - }